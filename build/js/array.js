var stringArr = ['one', 'hey'];
var guitars = ['Strat', 'Les Paul'];
var mixedData = ['EVH', 1984, true];
stringArr[0] = 'John';
stringArr.push('hey');
//guitars[0]=1984
guitars.unshift('Jim');
var test = [];
var bands = [];
bands.push('Van Halen');
var myTuple = ['Riya', 23, true];
var mixed = ['John', 1, false];
myTuple[1] = 42;
var myObj;
myObj = [];
console.log(typeof myObj);
myObj = bands;
myObj = {};
var exampleObj = {
    prop1: 'Dave',
    prop2: true
};
exampleObj.prop1 = 'John';
var evh = {
    name: 'riya',
    active: false,
    albums: [1984, 5150]
};
var jp = {
    name: 'Jimmy',
    active: true,
    albums: ['I', 'II', 'IV']
};
//evh = jp
var greet = function (guitarist) {
    return "Hello ".concat(guitarist.name, "!");
};
console.log(greet(jp));
//enums
var Grade;
(function (Grade) {
    Grade[Grade["U"] = 1] = "U";
    Grade[Grade["D"] = 2] = "D";
    Grade[Grade["C"] = 3] = "C";
    Grade[Grade["B"] = 4] = "B";
    Grade[Grade["A"] = 5] = "A";
})(Grade || (Grade = {}));
console.log(Grade.U);
