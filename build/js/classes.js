var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Coder = /** @class */ (function () {
    function Coder(name, music, age, lang) {
        this.name = name;
        this.music = music;
        this.age = age;
        this.lang = lang;
        this.name = name;
        this.music = music;
        this.age = age;
        this.lang = lang;
    }
    Coder.prototype.getAge = function () {
        return "Hello, I am ".concat(this.age);
    };
    return Coder;
}());
var Dave = new Coder('Dave', 'Rock', 42, 'language');
console.log(Dave.getAge());
// console.log(Dave.age) 
var WebDev = /** @class */ (function (_super) {
    __extends(WebDev, _super);
    function WebDev(computer, name, music, age) {
        var _this = _super.call(this, computer, name, music, age) || this;
        _this.computer = computer;
        _this.computer = computer;
        return _this;
    }
    WebDev.prototype.getLang = function () {
        return "I write ".concat(this.lang);
    };
    return WebDev;
}(Coder));
var Sara = new WebDev('Mac', 'Sara', 'Lofi', 25);
console.log(Sara.getLang());
var Guitar = /** @class */ (function () {
    function Guitar(name, instrument) {
        this.name = name;
        this.instrument = instrument;
    }
    Guitar.prototype.play = function (action) {
        return "".concat(this.name, " ").concat(action, " the ").concat(this.instrument);
    };
    return Guitar;
}());
var Page = new Guitar('Jimmy', 'guitar');
console.log(Page.play('strums'));
var Peeps = /** @class */ (function () {
    function Peeps(name) {
        this.name = name;
        this.name = name;
        this.id = ++Peeps.count;
    }
    Peeps.getCount = function () {
        return Peeps.count;
    };
    Peeps.count = 0;
    return Peeps;
}());
var John = new Peeps('John');
var Steve = new Peeps('Steve');
console.log(Steve.id);
console.log(Peeps.count);
var Bands = /** @class */ (function () {
    function Bands() {
        this.dataState = [];
    }
    Object.defineProperty(Bands.prototype, "data", {
        get: function () {
            return this.dataState;
        },
        set: function (value) {
            if (Array.isArray(value) && value.every(function (e1) { return typeof e1 === 'string'; })) {
                this.dataState = value;
                return;
            }
            else
                throw new Error('Param is not an array of strings');
        },
        enumerable: false,
        configurable: true
    });
    return Bands;
}());
var MyBands = new Bands();
MyBands.data = ['Neil Young', 'Led Zep'];
console.log(MyBands.data);
