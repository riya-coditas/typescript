var echo = function (arg) { return arg; };
var isObj = function (arg) {
    return (typeof arg === 'object' && !Array.isArray(arg) && arg !== null);
};
console.log(isObj(true));
console.log(isObj('John'));
console.log(isObj([1, 2, 3]));
var istrue = function (arg) {
    if (Array.isArray(arg) && !arg.length) {
        return { arg: arg, is: false };
    }
    if (isObj(arg) && !Object.keys(arg), length) {
        return { arg: arg, is: false };
    }
    return { arg: arg, is: !!arg };
};
console.log(istrue(true));
console.log(istrue(false));
console.log(istrue(1));
var checkBoolValue = function (arg) {
    if (Array.isArray(arg) && !arg.length) {
        return { value: arg, is: false };
    }
    if (isObj(arg) && !Object.keys(arg), length) {
        return { value: arg, is: false };
    }
    return { value: arg, is: !!arg };
};
var processUser = function (user) {
    return user;
};
console.log(processUser({ id: 1, name: "John" }));
var getUsersProperty = function (users, key) {
    return users.map(function (user) { return user[key]; });
};
var usersArray = [
    {
        "id": 1,
        "name": "Leane Graham",
        "userName": "Bret",
        "email": "Sincere@gmail.com",
        "address": {
            "street": "Kulas Light",
            "city": "Apt. 556",
            "geo": {
                "lat": "-37.37",
                "lang": "81.14"
            }
        }
    },
    {
        "id": 2,
        "name": "Erwin Howel",
        "userName": "Antonette",
        "email": "shana@gmail.com",
        "address": {
            "street": "Victor Plains",
            "city": "Wisko Burg",
            "geo": {
                "lat": "-43.9509",
                "lang": "-34.4618"
            }
        }
    },
];
console.log(getUsersProperty(usersArray, "email"));
console.log(getUsersProperty(usersArray, "userName"));
/////////////////////////////////////////////////////
var StateObject = /** @class */ (function () {
    function StateObject(value) {
        this.data = value;
    }
    Object.defineProperty(StateObject.prototype, "state", {
        get: function () {
            return this.data;
        },
        set: function (value) {
            this.data = value;
        },
        enumerable: false,
        configurable: true
    });
    return StateObject;
}());
var store = new StateObject("John");
console.log(store.state);
store.state = "Dave";
// store.state = 12
var myState = new StateObject([15]);
myState.state = (['Dave', 42, true]);
console.log(myState.state);
