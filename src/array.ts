let stringArr = ['one','hey']
let guitars = ['Strat','Les Paul']
let mixedData = ['EVH',1984,true]

stringArr[0] = 'John'
stringArr.push('hey')

//guitars[0]=1984
guitars.unshift('Jim')

let test = []
let bands:string[] =[]
bands.push('Van Halen')

let myTuple :[string ,number,boolean]=['Riya',23,true]
let mixed=['John',1,false]

myTuple[1] = 42

let myObj:object
myObj=[]
console.log(typeof myObj)
myObj = bands
myObj={}

const exampleObj={
    prop1:'Dave',
    prop2:true,
}

exampleObj.prop1 = 'John'

type Guitarist = {
    name:string,
    active:boolean,
    albums:(string|number)[]
}
let evh:Guitarist={
    name:'riya',
    active:false,
    albums:[1984,5150]
}

let jp:Guitarist={
    name:'Jimmy',
    active:true,
    albums:['I','II','IV']
}
//evh = jp

const greet=(guitarist:Guitarist)=>{
    return `Hello ${guitarist.name}!`
}
console.log(greet(jp))  

//enums
enum Grade{
    U = 1,
    D,
    C,
    B,
    A,
}
console.log(Grade.U);