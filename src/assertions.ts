type One = string
type Two = string | number
type Three ='hello'

let a1: One ='hello'
let b1  = a1 as Two // less specific
let c1 = a1 as Three // more specific

let d = <One>'world'
let e = <string |number>'world'

const addOrConcat = (a:number,b:number,c:'add'|'concat'):
number|string =>{
    if(c === 'add') return a + b 
    return '' + a + b
}

let myVal: string=addOrConcat(2,2,'concat') as string
let nextVal:number=addOrConcat(2,2,'concat') as number

//The Dom
const img = document.querySelector('img')!
const myImg =  document.getElementById('#img') as HTMLImageElement
const nextImg = <HTMLImageElement> document.getElementById('#img')

img.src
myImg.src