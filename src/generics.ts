const echo = <T>(arg: T) : T => arg

const isObj = <T>(arg : T): boolean => {
    return (typeof arg === 'object' && !Array.isArray(arg) && arg !==null)
}

console.log(isObj(true));
console.log(isObj('John'));
console.log(isObj([1,2,3]));

const istrue = <T>(arg : T): { arg: T, is : boolean} =>
{
    if(Array.isArray(arg) && !arg.length){
        return { arg, is: false}
    }
    if(isObj(arg) && !Object.keys(arg as keyof T),length){
        return {arg, is:false}
    }
    return {arg, is : !!arg}
}

console.log(istrue(true));
console.log(istrue(false));
console.log(istrue(1));


interface BoolCheck<T> {
    value: T,
    is: boolean
}

const checkBoolValue = <T>(arg : T): BoolCheck<T> =>
{
    if(Array.isArray(arg) && !arg.length){
        return { value: arg, is: false}
    }
    if(isObj(arg) && !Object.keys(arg as keyof T),length){
        return {value : arg, is:false}
    }
    return {value : arg, is : !!arg}
}

interface hasId {
    id: number
}

const processUser = <T extends hasId>(user : T): T => {
    return user
}

console.log(processUser({id: 1, name : "John" }))

const getUsersProperty = <T extends hasId, K extends keyof T>(users : T[], key : K): T[K][] => {
    return users.map((user) => user[key])
}

const usersArray = [

    {
        "id": 1,
        "name" : "Leane Graham",
        "userName" : "Bret",
        "email" : "Sincere@gmail.com",
        "address" : {
            "street" : "Kulas Light",
            "city": "Apt. 556",
            "geo" : {
                "lat" : "-37.37",
                "lang" : "81.14"
            }
        }
    },

    {
        "id": 2,
        "name" : "Erwin Howel",
        "userName" : "Antonette",
        "email" : "shana@gmail.com",
        "address" : {
            "street" : "Victor Plains",
            "city": "Wisko Burg",
            "geo" : {
                "lat" : "-43.9509",
                "lang" : "-34.4618"
            }
        }
    },
]

console.log(getUsersProperty(usersArray, "email"))

console.log(getUsersProperty(usersArray, "userName"))

/////////////////////////////////////////////////////

class StateObject<T> {

    private data : T

    constructor(value : T){
        this.data = value
    } 

    get state(): T{
        return this.data
    }

    set state(value : T){
        this.data = value
    }
}

const store = new StateObject("John")
console.log(store.state)
store.state = "Dave"
// store.state = 12


const myState = new StateObject<(string | number | boolean )[]>([15])
myState.state = (['Dave',42,true])

console.log(myState.state)